const { delDir, mkdirpSync } = require("./utils");
const extractCnLang = require("./extractCnLang");
const inputLangs = require("./inputLangs");
const fs = require("fs");

/**
 * 启动方法
 */
const getLang = async (context) => {
  delDir(context.distDir);
  for (let file of context.contextFiles) {
    mkdirpSync(file.distDir);
    const lang = await extractCnLang(file.fullPath);
    const langObj = inputLangs(lang);
    fs.writeFileSync(file.i18nFilePath, `export default ${langObj}`, "utf8");
  }
};
module.exports = getLang;

// const interceptFn = () => {};

/** 语言转化提取 */
function inputLangs(arr) {
  let item;
  let cs_Lang = {
    c: {},
  };
  let allObj = {};
  for (let val of arr) {
    if (allObj[val]) {
      allObj[val] = 2;
    } else {
      allObj[val] = 1;
    }
  }
  let otherObjs = {};

  // 提取公共
  let count = 0;
  for (let val in allObj) {
    if (allObj[val] === 2) {
      count++;
      cs_Lang["c"]["k_" + count] = val;
      otherObjs[val] = true;
    } else {
      continue;
    }
  }
  count = 0;
  cs_Lang[`p`] = {};
  for (let val of arr) {
    if (!otherObjs[val]) {
      count++;
      cs_Lang[`p`]["k_" + count] = val;
    } else {
      continue;
    }
  }
  let str = JSON.stringify(cs_Lang, null, "  ");
  return str;
}

module.exports = inputLangs;

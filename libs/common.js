const os = require("os");
module.exports = {
  test: /[\u4E00-\u9FA5\uF900-\uFA2D\u3001-\u3015]+[\u4E00-\u9FA5\uF900-\uFA2D\uff01\uff08-\uff1f\u3001-\u3015\u0020a-zA-Z\d\\\/+*/-]*/,
  dirU: os.type().toLowerCase().includes("window") ? "\\" : "/",
};

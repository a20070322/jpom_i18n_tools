const axios = require("axios");

const llmFetch = (key, content, context) => {
  return axios({
    // url: "https://api.moonshot.cn/v1/chat/completions",
    url: "http://127.0.0.1:3001",
    method: "POST",
    data: {
      model: "moonshot-v1-8k",
      messages: [
        // {
        //   role: "system",
        //   content:
        //     "你是 Kimi，由 Moonshot AI 提供的人工智能助手，你更擅长中文和英文的对话。你会为用户提供安全，有帮助，准确的回答。同时，你会拒绝一切涉及恐怖主义，种族歧视，黄色暴力等问题的回答。Moonshot AI 为专有名词，不可翻译成其他语言。",
        // },
        { role: "user", content: content },
      ],
      temperature: 0.3,
    },
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${key}`,
    },
  });
};

module.exports = llmFetch;

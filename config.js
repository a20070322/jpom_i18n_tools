const path = require("path");
const config = {
  // 执行目录
  cwd: "",
  // 项目所在目录
  projectPath: "/Users/zhaozhongyang/Desktop/Jpom/web-vue/src/pages",
  // 默认为 projectPath
  globalPath: "",
  // 包含的文件夹
  includeDir: ["node"],
  // 包含的文件如果注释则默认匹配文件夹下的所有文件
  // includeFile: ['repository-list.vue'],
  // 处理文件后缀
  exts: [".vue"],
  output: "dist",
  // tpl目录
  tplTranslate: path.join("tpl", "translate.txt"),
  tplTranslateEn: path.join("tpl", "translateEn.txt"),
  // 临时缓存目录
  distDir: "temp",
  // 中文提取目录
  translateDir: path.join("i18n", "zh-CN"),
  // 中文转英文目录
  translateEnDir: path.join("i18n", "en_US"),
  // 页面抽离替换目录
  pageDir: "pages",
  // 文本格式化 .prettierrc.json 配置
  prettier: {
    semi: false,
    singleQuote: true,
    endOfLine: "auto",
    proseWrap: "never",
    printWidth: 120,
    trailingComma: "none",
  },
};
module.exports = config;

const Koa = require("koa");
const Router = require("koa-router");
const bodyParser = require("koa-bodyparser");
const axios = require("axios");
const app = new Koa();
const router = new Router();
const fs = require("fs");
const path = require("path");
const getProxyToken = require("../libs/getProxyToken");
const tokenPath = path.join(process.cwd(), ".catch", "proxy-token");
let token = "";

const getToken = async () => {
  if (!fs.existsSync(tokenPath)) {
    token = await getProxyToken();
  } else {
    let payload = JSON.parse(fs.readFileSync(tokenPath, "utf-8"));
    if (payload.expire - 60 * 60 < Date.now() / 1000) {
      token = await getProxyToken();
    } else {
      token = payload.token;
    }
  }
};

async function errorMiddleware(ctx, next) {
  try {
    await next(); // 执行后代的代码

    if (!ctx.body) {
      // 没有资源，设置状态码为 404
      ctx.status = 404;
      console.log(ctx);
    }
  } catch (e) {
    // 如果后面的代码报错，返回 500
    ctx.status = 500;
  }
}

/** 请求封装 */
async function request(messages) {
  const { data } = await axios({
    // 切换至 ERNIE-3.5-8K 减少资金消耗
    url: `https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions?access_token=${token}`,
    // url: `https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions_pro?access_token=${token}`,
    // url: `https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions?access_token=${token}`,
    method: "post",
    headers: {
      "Content-Type": "application/json",
    },
    data: {
      messages,
    },
  });
  return data;
}

const main = async () => {
  await getToken();
  app.use(bodyParser());
  app.use(errorMiddleware);
  // 模拟 OpenAI 返回的数据
  router.post("/", async (ctx) => {
    try {
      console.log("====== 请求部分 ======");
      console.log(JSON.stringify(ctx.request.body));
      // fs.writeFileSync("request.json", ctx.request.body.messages[0]["content"]);
      const messages = ctx.request.body.messages;
      if (messages[0]["content"]) {
        messages[0][
          "content"
        ] = `//当前日期:${new Date().toLocaleDateString()}\n${
          messages[0].content
        }`;
      }
      const data = await request(messages);
      console.log("====== 返回部分 ======");
      if (!data.result) {
        console.log(data);
      }
      console.log(data.result);
      ctx.body = {
        choices: [
          {
            message: {
              content: data.result,
            },
          },
        ],
      };
    } catch (error) {
      console.log(error);
      ctx.body = "{}";
    }
  });

  app.use(router.routes());
  const PORT = process.env.PORT || 3001;
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
  });
};

main();

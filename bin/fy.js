const axios = require("axios");
const fs = require("fs");
const path = require("path");
const getFyToken = require("../libs/getFyToken");
let token = "";
const tokenPath = path.join(process.cwd(), ".catch", "fy-token");
const getToken = async () => {
  if (!fs.existsSync(tokenPath)) {
    token = await getFyToken();
  } else {
    let payload = JSON.parse(fs.readFileSync(tokenPath, "utf-8"));
    if (payload.expire - 60 * 60 < Date.now() / 1000) {
      token = await getFyToken();
    } else {
      token = payload.token;
    }
  }
};

const fyApi = async (text) => {
  const { data } = await axios({
    url: `https://aip.baidubce.com/rpc/2.0/mt/texttrans/v1?access_token=${token}`,
    method: "POST",
    data: {
      from: "auto",
      to: "en",
      q: text,
    },
  });
  return data.result.trans_result;
};

const main = async () => {
  await getToken();
  const cwd = process.cwd();
  try {
    const input = JSON.parse(
      fs.readFileSync(path.join(cwd, "bfy", "input.json"), "utf-8")
    );
    const output = {};
    for (let key in input) {
      const text = await fyApi(input[key]);
      output[key] = text[0]?.dst || "";
      console.log(`key ${key} done`);
    }
    fs.writeFileSync(
      path.join(cwd, "bfy", "output.json"),
      JSON.stringify(output, null, 2),
      "utf-8"
    );
  } catch (error) {
    console.log(error);
  }
};

main();

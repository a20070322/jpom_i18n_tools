const config = require("../config");
const createContext = require("../libs/createContext");
const translateLang = require("../libs/translateLang");
const translateEnLang = require("../libs/translateEnLang");
const getLang = require("../libs/getLang");
const replacePage = require("../libs/replacePage");
const createIndex = require("../libs/createIndex");

// 启动方法  createContext 必须保留 其他可以分步注释
const main = async (config) => {
  const context = await createContext(config);
  console.log("==== [success] createContext");
  await getLang(context);
  console.log("==== [success] getLang");
  await translateLang(context);
  console.log("==== [success] translateLang");
  await translateEnLang(context);
  console.log("==== [success] translateEnLang");
  await replacePage(context, 1);
  console.log("==== [success] replacePage");
  createIndex(context);
  console.log("==== [success] createIndex");
};
main(config);

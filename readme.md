# 命令介绍

## 启动项目

> 通过 config 文件配置，启动项目

```shell
pnpm run main
```

### 已知问题

1. 无法处理 page/xxx/index.vue 的情况

## pnpm run fy

> bfy/input.json -> bfy/output.json

```json
{
  "xx": "中文"
}
```

转换结果为

```json
{
  "xx": "Chinese"
}
```

## 启动代理服务

> 启动代理服务，使用百度大模型转发模拟为 openai 接口

```shell
pnpm run proxy
```
